package bcas.prg.ass;

import java.util.Scanner;

public class MoneyDeposit {

	public void moneyDeposit() {
		int i = 0;
		while (i < BankAccount.depositCount) {
			System.out.println("Enter The amount of Deposit #" + (i + 1) + " : ");
			Scanner scan = new Scanner(System.in);
			double amount = scan.nextDouble();

			if (amount <= 0) {
				System.err.println("deposit must be greater than zero, Try Again! ");
				moneyDeposit();
			} else {
				BankAccount.AccountBalance += amount;
			}
			i++;
		}
	}

}
