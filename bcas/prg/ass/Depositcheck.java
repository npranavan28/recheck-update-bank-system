package bcas.prg.ass;

import java.util.Scanner;

public class Depositcheck {

	void depositCount() {
		System.out.println("Enter the Number of deposit [0-5] : ");
		Scanner scanner = new Scanner(System.in);
		int Count = scanner.nextInt();

		if (Count > 5 || Count < 0) {
			System.err.println("Invalid number of deposits, Maximum 5 Deposit");
			depositCount();
		} else {
			BankAccount.depositCount = Count;
		}
	}
}