package bcas.prg.ass;

import java.util.Scanner;

public class Bank {

	public static void main(String[] args) {
		BankAccount boc = new BankAccount();
		BalanceCheck bCheck = new BalanceCheck();
		Depositcheck depositcheck = new Depositcheck();
		Withdrawcountcheck withdrawcountcheck = new Withdrawcountcheck();
		MoneyDeposit mDeposit = new MoneyDeposit();
		MoneyWithdrawl mWithdrawl = new MoneyWithdrawl();
		String state;
		do {
			System.out.println("---------------------------------------");
			System.out.println("***** Welcome to BOC *****");
			System.out.println("---------------------------------------");

			bCheck.balance();
			depositcheck.depositCount();
			withdrawcountcheck.withdrawalsCount();
			mDeposit.moneyDeposit();
			System.out.println("*** The Current Balance is Rs." + boc.AccountBalance);
			mWithdrawl.moneywithdrawal();
			System.out.println("*** The Closing Balance is Rs." + boc.AccountBalance);

			if (boc.AccountBalance >= 50000) {
				System.out.println("It's time to invest some money ");
			} else if (boc.AccountBalance >= 15000) {
				System.out.println("you shouls consider CD");
			} else if (boc.AccountBalance >= 1000) {
				System.out.println("keep up the good work");
			} else {
				System.err.println("your Balance is getting low");
			}
			System.out.println("do u want to continue ... y = yes or n = no");
			Scanner scan = new Scanner(System.in);

			state = scan.nextLine();
			if (state.equals("n")) {
				System.out.println("EXIT");
			}
		} while (state.equals("y"));

	}

}