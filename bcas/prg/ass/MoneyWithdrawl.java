package bcas.prg.ass;

import java.util.Scanner;

public class MoneyWithdrawl {

	public void moneywithdrawal() {
		int i = 0;

		while (i < BankAccount.withdrawlCount) {

			System.out.println("Enter The amount of Withdrawal #" + i + " : ");

			Scanner scan = new Scanner(System.in);

			double amount = scan.nextDouble();

			if (amount > BankAccount.AccountBalance) {

				System.err.println("Withdrawal amount exceeds current balance, Try Again! ");

				moneywithdrawal();

			} else {

				BankAccount.AccountBalance -= amount;

			}
			i++;
		}
	}
}
