
import java.util.Scanner;



public class IdentifyPrimenumber {

	public static void main(String args[]) {

		long temp;
		String state = "";
	
		do {
			Scanner scan = new Scanner(System.in);

			boolean isPrime = true;
			System.out.println("Enter any number:");
			
			long num = scan.nextLong();
			
			if(num==0 || num==1) {
				isPrime = false;
			}else {
			
			
			for (int i = 2; i <= num / 2; i++) {
				temp = num % i;
				
				if (temp == 0 || num == 1 ) {
					isPrime = false;
					break;
				}}
			}
			
			if (isPrime )
				System.out.println(num + " is a Prime Number");
			else
				System.out.println(num + " is not a Prime Number");

			System.out.println("do u want to continue ... y = yes or n = no");
			state = scan.next();
			if (state.equals("n")) {
				System.out.println("EXIT");
			}
		} while (state.equals("y"));

	}
}
